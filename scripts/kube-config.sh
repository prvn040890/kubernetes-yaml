for i in `cat nodes.txt`; 
do ssh ubuntu@$i "mkdir .kube"; 
scp /home/ubuntu/.kube/config ubuntu@$i:/home/ubuntu/.kube ;
scp /home/ubuntu/kubernetes-yaml/scripts/settings.xml ubuntu@$i:/home/ubuntu/settings.xml
done
