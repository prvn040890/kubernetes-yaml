#! /bin/bash

FS_NAME=`aws efs describe-file-systems --output text | grep -v SIZEINBYTES |awk '{print $5}'`
DNS_NAME="$FS_NAME.efs.us-east-1.amazonaws.com"
sed -i "s/FS_NAME/$FS_NAME/g" /home/ubuntu/kubernetes-yaml/efs/efs-manifest.yaml
sed -i "s/DNS_NAME/$DNS_NAME/g" /home/ubuntu/kubernetes-yaml/efs/efs-manifest.yaml
