#! /bin/bash
HASH=`tail -1 /tmp/install.txt | awk '{print $2}'`
TOKEN=`tail -2 /tmp/install.txt |  head -1 | awk '{print $5}'`
ENDPOINT=`tail -2 /tmp/install.txt | head -1 | awk '{print $3}'`
hn=`curl http://169.254.169.254/latest/meta-data/local-hostname`
sed -i "s/HASH/$HASH/g" /home/ubuntu/kubernetes-yaml/join.yaml
sed -i "s/TOKEN/$TOKEN/g" /home/ubuntu/kubernetes-yaml/join.yaml
sed -i "s/ENDPOINT/$ENDPOINT/g" /home/ubuntu/kubernetes-yaml/join.yaml
sed -i "s/SERVER/$hn/g" /home/ubuntu/kubernetes-yaml/join.yaml